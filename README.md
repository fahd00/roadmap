# README #

Ce fichier README documentera normalement toutes les étapes nécessaires comprendre le tableau RoadMap.

### A quoi sert ce README ? ###

* Ce README est une formalisation du tableau blanc au bureau pour faciliter l'acces et bien comprendre nos objectif!
<img src="https://bitbucket.org/kiwipdevteam/roadmap/downloads/20211229_094023.jpg" alt="InfiniteGraph Logo" width="220">

### Comment utiliser? ###

* Nous aurons 3 fichiers :
* [Bug]
* [New feature]
* [Nice to have]
* Le premier dossier contiendra tous les bugs que nous avons découverts et que nous n'avons pas encore corrigés.
* Le deuxième dossier contiendra toutes les fonctionnalités que nous avons en tête mais que nous n'avons pas encore terminées.
* Le troisième dossier contiendra toutes les fonctionnalités que nous souhaitons ajouter à l'avenir.